function createCard(name, description, pictureUrl) {
    return `
      <div class="card, shadow p-3 mb-4 bg-body rounded">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <p class="card-text">${description}</p>
        </div>
      </div>
    `;
  }

window.addEventListener('DOMContentLoaded', async () => {
  const url = 'http://localhost:8000/api/conferences/';
  try {
    const response = await fetch(url);
    if (!response.ok) {
        throw new Error('Response not ok');
    } else {
        const data = await response.json();
        let i = 1;
        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const html = createCard(title, description, pictureUrl);
            
            const colTag = document.querySelector(`div#col${i}`);
            colTag.innerHTML += html;
            if (i === 3){
                i = 1;
            }
            else{
                i++;
            }
          }
        }
    }
  } catch (e) {
    console.error('error', e);
  }
});
